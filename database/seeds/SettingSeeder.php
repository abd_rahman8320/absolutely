<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')
        ->insert([
            'google_api_key' => 'AIzaSyCyII53enek1EdoEqmVvepVNTJmidatrec',
            'instagram' => 'https://www.instagram.com/absolute_pictures/',
            'youtube' => 'https://www.youtube.com/channel/UCGiZk-t5wQ6V-nX7UQR0giQ'
        ]);
    }
}
