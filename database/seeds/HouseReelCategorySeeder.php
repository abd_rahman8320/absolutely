<?php

use Illuminate\Database\Seeder;

class HouseReelCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'Main Category',
            'order' => 1
        ];

        DB::table('house_reel_categories')
        ->insert($data);
    }
}
