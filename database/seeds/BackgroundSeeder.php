<?php

use Illuminate\Database\Seeder;

class BackgroundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'section' => 'about',
                'filename' => '2nd-section.jpg'
            ],
            [
                'section' => 'contact-us',
                'filename' => '5th-section.jpg'
            ]
        ];
        DB::table('backgrounds')
        ->insert($data);
    }
}
