<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUrlOnHouseReels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('house_reels', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('image');
            $table->string('title')->after('category_id');
            $table->string('url')->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('house_reels', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('url');
            $table->string('name')->after('category_id');
            $table->string('image')->after('name');
        });
    }
}
