@extends('admin.index', ['title' => 'Admin | House Reel'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Input House Reel</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/house-reel')}}" method="POST" enctype="multipart/form-data" class="row">
            {{csrf_field()}}
            <div class="col-6">
                <div class="form-group">
                    <label for="cateogry">Category</label>
                    <div class="row">
                        <select name="category" id="category" class="form-control col-9" style="margin-left:1rem">
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>

                        <input type="text" name="category_name" value="" class="form-control col-9" id="category_name" style="display:none;margin-left:1rem;">

                        <button type="button" class="btn btn-primary offset-1" title="New Category" id="btn-new">
                            <i class="fas fa-plus"></i>
                        </button>

                        <button type="button" class="btn btn-danger offset-1" title="New Category" id="btn-cancel" style="display:none;">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                    
                </div>

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" value="" class="form-control">
                </div>

                <div class="form-group">
                    <label for="url">URL</label>
                    <input type="text" name="url" value="" class="form-control" required>
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label for="order">Order</label>
                    <input type="number" name="order" value="{{$order}}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="1" selected>Active</option>
                        <option value="0">Non-Active</option>
                    </select>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@stop

@section('content_script')
<script>
$('#btn-new').click(function(){
    $('#btn-new').hide();
    $('#category').hide();

    $('#btn-cancel').show();
    $('#category_name').show();

    $('#category').val('');
});

$('#btn-cancel').click(function(){
    $('#btn-new').show();
    $('#category').show();

    $('#btn-cancel').hide();
    $('#category_name').hide();

    $('#category_name').val('');
});
</script>
@stop