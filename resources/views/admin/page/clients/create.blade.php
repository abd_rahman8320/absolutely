@extends('admin.index', ['title' => 'Admin | Clients'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Input Client</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/clients')}}" method="POST" enctype="multipart/form-data" class="col-3">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="">
            </div>

            <div class="form-group">
                <label for="title">Image</label>
                <input type="file" name="image" value="Upload File" class="form-control" accept="image/*">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@stop

@section('content_script')
<script>
$('#btn-new').click(function(){
    $('#btn-new').hide();
    $('#category').hide();

    $('#btn-cancel').show();
    $('#category_name').show();

    $('#category').val('');
});

$('#btn-cancel').click(function(){
    $('#btn-new').show();
    $('#category').show();

    $('#btn-cancel').hide();
    $('#category_name').hide();

    $('#category_name').val('');
});
</script>
@stop