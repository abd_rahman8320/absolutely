@extends('admin.index', ['title' => 'Admin | Settings'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Edit Settings</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/settings/'.$data->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="google_api_key">Google API Key:</label>
                <input type="text" name="google_api_key" value="{{$data->google_api_key}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="instagram">Instagram:</label>
                <input type="text" name="instagram" value="{{$data->instagram}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="youtube">Youtube:</label>
                <input type="text" name="youtube" value="{{$data->youtube}}" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@stop