@extends('admin.index', ['title' => 'Admin | Settings'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/change-password')}}" method="POST" enctype="multipart/form-data" id="form-cp">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="old_password">Old Password:</label>
                <input type="password" name="old_password" value="" class="form-control">
            </div>

            <div class="form-group">
                <label for="new_password">New Password:</label>
                <input type="password" name="new_password" value="" class="form-control" id="np">
            </div>

            <div class="form-group">
                <label for="new_password_confirmation">New Password Confirmation:</label>
                <input type="password" name="new_password_confirmation" value="" class="form-control" id="npc">
            </div>

            <button type="button" class="btn btn-primary" onclick=check()>Submit</button>
        </form>
    </div>
</div>
@stop

@section('content_script')
<script>

function check()
{
    var np = $('#np').val();
    var npc = $('#npc').val();

    if (np != npc) {
        alert('Please, make sure your confirmation is right');
    }else{
        $('#form-cp').submit();
    }
}

</script>
@stop