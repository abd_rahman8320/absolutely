@extends('admin.index', ['title' => 'Admin | Carousel'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Input Carousel</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/carousel')}}" method="POST" enctype="multipart/form-data" class="col-3">
            {{csrf_field()}}
            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" value="Upload File" class="form-control" accept="image/*">
            </div>

            <div class="form-group">
                <label for="order">Order</label>
                <input type="number" name="order" value="{{$order}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="status">
                    <option value="1" selected>Active</option>
                    <option value="0">Non-Active</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@stop