<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; {{date('Y')}} Absolutely - Developed by: <a rel="nofollow noopener" href="https://www.linkedin.com/in/abdur-rahman-63818217b/"><em>AR</em></a> & <a rel="nofollow noopener" href="https://www.linkedin.com/in/fadlanzunima/"><em>FZ</em></a></p></span>
        </div>
    </div>
</footer>