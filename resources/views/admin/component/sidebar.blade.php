<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin-panel">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Absolutely Admin</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    @if(Request::is('admin-panel/carousel') || Request::is('admin-panel/carousel/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/carousel')}}">
            <i class="fas fa-fw fa-image"></i>
            <span>Carousel</span></a>
    </li>

    @if(Request::is('admin-panel/house-reel') || Request::is('admin-panel/house-reel/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/house-reel')}}">
            <i class="fas fa-fw fa-home"></i>
            <span>House Reel</span></a>
    </li>

    @if(Request::is('admin-panel/clients') || Request::is('admin-panel/clients/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/clients')}}">
            <i class="fas fa-fw fa-user-friends"></i>
            <span>Clients</span></a>
    </li>

    @if(Request::is('admin-panel/award') || Request::is('admin-panel/award/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/award')}}">
            <i class="fas fa-fw fa-trophy"></i>
            <span>Awards</span></a>
    </li>

    @if(Request::is('admin-panel/messages') || Request::is('admin-panel/messages/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/messages')}}">
            <i class="fas fa-fw fa-envelope"></i>
            <span>Messages</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Settings
    </div>

    @if(Request::is('admin-panel/settings') || Request::is('admin-panel/settings/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/settings')}}">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span></a>
    </li>

    @if(Request::is('admin-panel/change-password') || Request::is('admin-panel/change-password/*'))
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{url('admin-panel/change-password')}}">
            <i class="fas fa-fw fa-cog"></i>
            <span>Change Password</span></a>
    </li>

</ul>
<!-- End of Sidebar -->