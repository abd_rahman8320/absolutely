<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if($status == 'landing')
                    <div class="primary-button">
                        <a href="#home" style="background-color: #1176bc;">Kembali ke Atas</a>
                    </div>
                    @endif
                    <ul>
                        <li><a href="{{$setting->instagram}}" target="_blank" style="background-color: #1176bc;"><i class="fa fa-instagram" style="color: white;"></i></a></li>
                        <li><a href="{{$setting->youtube}}" target="_blank" style="background-color: #1176bc;"><i class="fa fa-youtube-play" style="color: white;"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-google"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li> -->
                    </ul>
                    <p class="font-pragmatica">Copyright &copy; {{date('Y')}} Absolutely

            		- Developed by: <a rel="nofollow noopener" href="https://www.linkedin.com/in/abdur-rahman-63818217b/"><em>AR</em></a> & <a rel="nofollow noopener" href="https://www.linkedin.com/in/fadlanzunima/"><em>FZ</em></a></p>
                </div>
            </div>
        </div>
    </footer>