<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" id="button_modal_video{{$id}}" data-toggle="modal" data-target="#modal_video{{$id}}" hidden>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="modal_video{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg mt-5vw" role="document">
    <div class="modal-content custom-bg-modal">
      <div class="d-flex justify-content-between">
        <span></span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="btn-closed-sign">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card">
            <iframe class="wrap-video" src="{{$url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>