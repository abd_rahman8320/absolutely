<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Absolutely</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Abdurrahman & Fadlan Zunima">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="{{url('css/style.css')}}">

    <link rel="stylesheet" href="{{url('vanilla/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('vanilla/css/fontAwesome.css')}}">
    <link rel="stylesheet" href="{{url('vanilla/css/hero-slider.css')}}">
    <link rel="stylesheet" href="{{url('vanilla/css/templatemo-main.css')}}">
    <link rel="stylesheet" href="{{url('vanilla/css/owl-carousel.css')}}">

    <link rel="stylesheet" href="{{url('aos/dist/aos.css')}}" />

    <!-- SLICK -->
    <link rel="stylesheet" type="text/css" href="{{url('slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('slick/slick-theme.css')}}"/>
    <!-- SLICK -->
    <!-- AOS -->
    <script src="{{url('aos/dist/aos.js')}}"></script>
    <!-- AOS -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <script src="{{url('vanilla/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
</head>