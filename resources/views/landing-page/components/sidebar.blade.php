<div class="fixed-side-navbar">
    <ul class="nav flex-column">
        <li class="nav-item"><a class="nav-link" href="#home"><span>Beranda</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#services"><span>Tentang Kami</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#house-reel"><span>Reels Kami</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#award"><span>Award Kami</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#our-clients"><span>Klien Kami</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#contact-us"><span>Hubungi Kami</span></a></li>
    </ul>
</div>