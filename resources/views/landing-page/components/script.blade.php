<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{url("vanilla/js/vendor/jquery-1.11.2.min.js")}}"><\/script>')</script>
<script src="{{url('vanilla/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{url('vanilla/js/plugins.js')}}"></script>
<script src="{{url('vanilla/js/main.js')}}"></script>
<!-- SLICK -->
<script type="text/javascript" src="{{url('slick/slick.min.js')}}"></script>
<!-- SLICK -->
<script>
    function openCity(cityName) {
        var i;
        var x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        document.getElementById(cityName).style.display = "block";
    }
</script>

<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links
        $(".fixed-side-navbar a, .primary-button a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
</script>

<script>
    $(".slider-image").slick({
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    AOS.init();

    function showModal(id){
        $('#button_modal_video'+id).click()
    }

    function sendMessage()
    {
        if ($('#email').val() != '' && $('#message').val() != '') {
            $.ajax({
                url: '/messages',
                method: 'POST',
                data: {
                    name: $('#name').val(),
                    email: $('#email').val(),
                    message: $('#message').val(),
                    _token: $('#token').val()
                },
                success: function(result) {
                    alert('Your Message Has Been Sent');
                    location.reload();
                },
                error: function(err) {
                    alert('Failed. Please, Try Again Later');
                }
            });
        }else{
            $('#btn-submit').click();
        }
        
    }
</script>