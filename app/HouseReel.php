<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseReel extends Model
{
    public function category()
    {
        return $this->belongsTo('App\HouseReelCategory', 'category_id');
    }
}
