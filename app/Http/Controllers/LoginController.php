<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function loginProcess(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $user = User::where('username', $username)->first();

        if ($user == null) {
            return redirect('login');
        }

        if (Hash::check($password, $user->password)) {
            session(['user_id' => $user->id]);

            return redirect('admin-panel');
        }else {
            return redirect('login');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('login');
    }

    public function changePassword(Request $request)
    {
        $new_password = $request->input('new_password');
        $old_password = $request->input('old_password');

        $user = User::find(session('user_id'));
        
        if (!Hash::check($old_password, $user->password)) {
            return 'Your Old Password is Wrong';
        }

        $user->password = Hash::make($new_password);
        $user->save();

        return redirect('login');
    }

    public function viewChangePassword()
    {
        return view('admin.page.change_password.main');
    }
}
