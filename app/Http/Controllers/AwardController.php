<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Award;
use App\AwardCategory;
use DB;
use App\Traits\GlobalTrait;

class AwardController extends Controller
{
    use GlobalTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Award::get();

        return view('admin.page.award.main', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = AwardCategory::get();

        return view('admin.page.award.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = $request->input('category');
        $category_name = $request->input('category_name');
        $name = $request->input('name');
        $image = $request->file('image');
        $award = $request->input('award');
        $title = $request->input('title');
        $brand_client = $request->input('brand_client');
        $agency = $request->input('agency');
        $director = $request->input('director');

        try {
            DB::beginTransaction();

            $destination = 'assets/award/';
            $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
            $upload = $image->move($destination, $filename);

            $source = $destination.$filename;

            if ($category_name != '') {
                $cat = new AwardCategory;
                $cat->name = $category_name;
                $cat->order = AwardCategory::max('order') + 1;
                $cat->save();
                $category = $cat->id;
            }else {
                $check_category = AwardCategory::find($category);
                if ($check_category == null) {
                    return 'Kategori Tidak Ditemukan. Masukan Kategori Terlebih Dahulu';
                }
            }

            $award_input = new Award;
            $award_input->award_category_id = $category;
            $award_input->name = $name;
            $award_input->image = $filename;
            $award_input->award = $award;
            $award_input->title = $title;
            $award_input->brand_client = $brand_client;
            $award_input->agency = $agency;
            $award_input->director = $director;
            $award_input->save();

            DB::commit();

            return redirect('admin-panel/award');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Award::find($id);

        return view('admin.page.award.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $request->input('category');
        $category_name = $request->input('category_name');
        $name = $request->input('name');
        $image = $request->file('image');
        $award = $request->input('award');
        $title = $request->input('title');
        $brand_client = $request->input('brand_client');
        $agency = $request->input('agency');
        $director = $request->input('director');

        try {
            DB::beginTransaction();

            $destination = 'assets/award/';
            $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
            $upload = $image->move($destination, $filename);

            $source = $destination.$filename;

            if ($category_name != '') {
                $cat = new AwardCategory;
                $cat->name = $category_name;
                $cat->order = AwardCategory::max('order') + 1;
                $cat->save();
                $category = $cat->id;
            }else {
                $check_category = AwardCategory::find($category);
                if ($check_category == null) {
                    return 'Kategori Tidak Ditemukan. Masukan Kategori Terlebih Dahulu';
                }
            }

            $award_input = new Award;
            $award_input->award_category_id = $category;
            $award_input->name = $name;
            $award_input->image = $filename;
            $award_input->award = $award;
            $award_input->title = $title;
            $award_input->brand_client = $brand_client;
            $award_input->agency = $agency;
            $award_input->director = $director;
            $award_input->save();

            DB::commit();

            return redirect('admin-panel/award');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
