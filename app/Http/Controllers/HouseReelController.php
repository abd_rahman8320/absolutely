<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HouseReel;
use App\HouseReelCategory;
use App\Traits\GlobalTrait;
use DB;

class HouseReelController extends Controller
{
    use GlobalTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = HouseReel::orderBy('order', 'asc')->get();

        return view('admin.page.housereel.main', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = HouseReelCategory::get();
        $order = HouseReel::max('order') + 1;

        return view('admin.page.housereel.create', [
            'categories' => $categories,
            'order' => $order
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = $request->input('category');
        $category_name = $request->input('category_name');
        $title = $request->input('title');
        $url = $request->input('url');
        $order = $request->input('order');
        $status = $request->input('status');

        try {
            DB::beginTransaction();

            if ($category_name != '') {
                $cat = new HouseReelCategory;
                $cat->name = $category_name;
                $cat->order = HouseReelCategory::max('order') + 1;
                $cat->save();
                $category = $cat->id;
            }else {
                $check_category = HouseReelCategory::find($category);
                if ($check_category == null) {
                    return 'Kategori Tidak Ditemukan. Masukan Kategori Terlebih Dahulu';
                }
            }

            $check_order = HouseReel::where('order', $order)->first();
            if ($check_order != null) {
                $check_order->order = HouseReel::max('order') + 1;
                $check_order->save();
            }

            $match = [];
            preg_match("/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/", $url, $match);

            $video_info = $this->getYoutubeInfo(['id' => $match[2]]);
            
            if (isset($video_info->items[0]->snippet->thumbnails->maxres->url)) {
                $thumbnail = $video_info->items[0]->snippet->thumbnails->maxres->url;
            }elseif (isset($video_info->items[0]->snippet->thumbnails->standard->url)) {
                $thumbnail = $video_info->items[0]->snippet->thumbnails->standard->url;
            }elseif (isset($video_info->items[0]->snippet->thumbnails->high->url)) {
                $thumbnail = $video_info->items[0]->snippet->thumbnails->high->url;
            }elseif (isset($video_info->items[0]->snippet->thumbnails->medium->url)) {
                $thumbnail = $video_info->items[0]->snippet->thumbnails->medium->url;
            }elseif (isset($video_info->items[0]->snippet->thumbnails->default->url)) {
                $thumbnail = $video_info->items[0]->snippet->thumbnails->default->url;
            }
            
            $url = 'https://www.youtube.com/embed/'.$video_info->items[0]->id;

            $house_reel = new HouseReel;
            $house_reel->category_id = $category;
            $house_reel->title = $title;
            $house_reel->url = $url;
            $house_reel->thumbnail = $thumbnail;
            $house_reel->order = ($order != null && $order != 0) ? $order : HouseReel::max('order') + 1;
            $house_reel->status = ($status != null) ? $status : 1;
            $house_reel->save();

            DB::commit();

            return redirect('admin-panel/house-reel');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $house_reel = HouseReel::with('category')->find($id);
        $categories = HouseReelCategory::get();

        return view('admin.page.housereel.edit', [
            'house_reel' => $house_reel,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $request->input('category');
        $category_name = $request->input('category_name');
        $title = $request->input('title');
        $url = $request->input('url');
        $order = $request->input('order');
        $status = $request->input('status');

        try {
            DB::beginTransaction();

            if ($category_name != '') {
                $cat = new HouseReelCategory;
                $cat->name = $category_name;
                $cat->order = HouseReelCategory::max('order') + 1;
                $cat->save();
                $category = $cat->id;
            }elseif($category != '') {
                $check_category = HouseReelCategory::find($category);
                if ($check_category == null) {
                    return 'Kategori Tidak Ditemukan. Masukan Kategori Terlebih Dahulu';
                }
            }

            $house_reel = HouseReel::find($id);

            $check_order = HouseReel::where('order', $order)->first();
            if ($check_order != null) {
                $check_order->order = $house_reel->order;
                $check_order->save();
            }

            if ($url != null) {
                $match = [];
                preg_match("/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/", $url, $match);

                $video_info = $this->getYoutubeInfo(['id' => $match[2]]);
                
                if (isset($video_info->items[0]->snippet->thumbnails->maxres->url)) {
                    $thumbnail = $video_info->items[0]->snippet->thumbnails->maxres->url;
                }elseif (isset($video_info->items[0]->snippet->thumbnails->standard->url)) {
                    $thumbnail = $video_info->items[0]->snippet->thumbnails->standard->url;
                }elseif (isset($video_info->items[0]->snippet->thumbnails->high->url)) {
                    $thumbnail = $video_info->items[0]->snippet->thumbnails->high->url;
                }elseif (isset($video_info->items[0]->snippet->thumbnails->medium->url)) {
                    $thumbnail = $video_info->items[0]->snippet->thumbnails->medium->url;
                }elseif (isset($video_info->items[0]->snippet->thumbnails->default->url)) {
                    $thumbnail = $video_info->items[0]->snippet->thumbnails->default->url;
                }
                
                $url = 'https://www.youtube.com/embed/'.$video_info->items[0]->id;
            }

            $house_reel->category_id = ($category != null) ? $category : $house_reel->category_id;
            $house_reel->title = ($title != null) ? $title : $house_reel->title;
            $house_reel->url = ($url != null) ? $url : $house_reel->url;
            $house_reel->thumbnail = (isset($thumbnail)) ? $thumbnail : $house_reel->thumbnail;
            $house_reel->order = ($order != null && $order != 0) ? $order : $house_reel->order;
            $house_reel->status = ($status != null) ? $status : $house_reel->status;
            $house_reel->save();

            DB::commit();

            return redirect('admin-panel/house-reel');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $house_reel = HouseReel::find($id);
        $house_reel->delete();

        return redirect('admin-panel/house-reel');
    }
}
